package ca.csf.mobile1.tp2.model;

/**
 * Created by Jeammy on 29/03/17.
 */

/**
 * S'occupe de substituer les lettre de nôtre string.
 */
public class ComputeEncryption {
    private final char[] alphabet;
    private final char[] substitution;
    private final String text;

    //BEN_CORRECTION : C'est pas une bonne idée d'envoyer la chaine à chiffrer ou déchiffrer en paramètre
    //                 de la construction. Il aurait du être envoyé en paramêtre des deux méthodes "encrypt"
    //                 et "decrypt".
    //
    //                 Le problème ici, c'est qu'il devient inutilisable pour crypter autre chose que le texte
    //                 envoyé à la construction.
    //
    //                 De plus, pourquoi ne pas avoir fait de la composition entre "ComputeEncryption" et
    //                 "SubstitutionCypherKey" ? SubstitutionCypherKey contient déjà tout, alors pourquoi le
    //                 copier intégralement dans "ComputeEncryption" ? Perte de temps CPU.
    public ComputeEncryption(final char[] alphabet,final char[] substitution,final String text){
        this.alphabet = alphabet;
        this.substitution = substitution;
        this.text = text;
    }


    public String encrypt(){
        String encryptedString = "";
        for (int i = 0; i < text.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if(alphabet[j] == text.charAt(i)){
                    encryptedString += substitution[j];
                }
            }
        }
        return encryptedString;
    }

    public String decrypt(){
        String decryptedString = "";
        for (int i = 0; i < text.length(); i++) {
            for (int j = 0; j < substitution.length; j++) {
                if (substitution[j] == text.charAt(i)) {
                    decryptedString += alphabet[j];
                }
            }
        }
        return decryptedString;
    }
}
